# Tjek Slagelse
![alt text](https://focuslock.dk/xamarin/tjek_slagelse_original.png)

<h2>Om Tjek Slagelse</h2>
Tjek Slagelse er lavet for at Slagelse Kommune kan trække data om bygning- & udvikningsplaner på en moderne måde.

<h2>Tekninkken</h2>
Tjek Slagelse er en Cross-Platform app til Android og iPhone, appen er lavet med Xamarin.Forms framework.

<h2>Bagved Tjek Slagelse</h2>
Appen er udviklet af Sebastian Berg Rasmussen og Malte Gejr Korup.

<h6>Gejr[s]</h6>
