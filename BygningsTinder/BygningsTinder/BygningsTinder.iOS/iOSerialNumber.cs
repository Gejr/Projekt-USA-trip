﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BygningsTinder.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(iOSerialNumber))]
namespace BygningsTinder.iOS
{
    class iOSerialNumber : IGetSerialNumber
    {
        public string GetSerialNumber()
        {
            return UIDevice.CurrentDevice.IdentifierForVendor.AsString();
        }
    }

}