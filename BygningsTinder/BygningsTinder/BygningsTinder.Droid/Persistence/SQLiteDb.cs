﻿using System;
using System.IO;
using Xamarin.Forms;
using BygningsTinder.Droid;
using SQLite;

[assembly: Dependency(typeof(SQLiteDb))]

namespace BygningsTinder.Droid
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteAsyncConnection GetConnection()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var path = Path.Combine(documentsPath, "MySQLite.db3");

            return new SQLiteAsyncConnection(path);
        }
    }
}
