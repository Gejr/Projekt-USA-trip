﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using BygningsTinder.Droid;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(Button), typeof(FlatButtonRenderer))]
namespace BygningsTinder.Droid
{
    public class FlatButtonRenderer : ButtonRenderer
    {
        protected override void OnDraw(Android.Graphics.Canvas canvas)
        {
            base.OnDraw(canvas);
        }
    }
}