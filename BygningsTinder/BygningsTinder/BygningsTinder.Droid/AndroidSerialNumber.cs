﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BygningsTinder.Droid;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidSerialNumber))]
namespace BygningsTinder.Droid
{
    class AndroidSerialNumber : IGetSerialNumber
    {
        public string GetSerialNumber()
        {
            return Android.OS.Build.Serial;
        }
    }

}