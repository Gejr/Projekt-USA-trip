﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BygningsTinder.Droid
{
    public class ReadWriteFile
    {
        static string pathToDocuments = Android.OS.Environment.DirectoryDownloads;
        string filename = Path.Combine(pathToDocuments, "results.txt");

        public void WriteToFile()
        {
            using (var sw = new StreamWriter(filename, true))
            {
                sw.WriteLine(DateTime.UtcNow);
            }
        }
    }
}