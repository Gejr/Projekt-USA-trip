﻿using System;
using System.IO;
using System.Net;
using BygningsTinder.Droid;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(FTP))] //You need to put this on iOS/droid class or uwp/etc if you wrote
namespace BygningsTinder.Droid
{
    class FTP : IFtpWebRequest
    {
        public FTP() //I saw on Xamarin documentation that it's important to NOT pass any parameter on that constructor
        {
        }

        /// Upload File to Specified FTP Url with username and password and Upload Directory if need to upload in sub folders
        ///Base FtpUrl of FTP Server
        ///Local Filename to Upload
        ///Username of FTP Server
        ///Password of FTP Server
        ///[Optional]Specify sub Folder if any
        /// Status String from Server
        public void upload(string file)
        {
            Console.WriteLine(file);

            var request = (FtpWebRequest)WebRequest.Create("ftp://ftp.focuslock.dk/ftp/"+Android.OS.Build.Serial + ".txt");

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential("focuslock.dk", "bagebe");
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            using (var filestream = File.OpenRead(file))
            {
                using (var requeststream = request.GetRequestStream())
                {
                    filestream.CopyTo(requeststream);
                    requeststream.Close();
                }
            }

            var response = (FtpWebResponse)request.GetResponse();
            Console.WriteLine("Upload done {0}", response.StatusDescription);
            response.Close();

        }
    }
}