﻿using System;
using BygningsTinder.Droid;
using Xamarin.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Dependency(typeof(SaveLoadFiles))]
namespace BygningsTinder.Droid
{
    public class SaveLoadFiles : ISaveAndLoad
    {
        public void SaveText(string filename, string text)
        {
            var documentsPath = Android.OS.Environment.DirectoryDocuments;
            var filePath = Path.Combine(documentsPath, filename);
            System.IO.File.WriteAllText(filePath, text);
        }
        public string LoadText(string filename)
        {
            var documentsPath = Android.OS.Environment.DirectoryDocuments;
            var filePath = Path.Combine(documentsPath, filename);
            return System.IO.File.ReadAllText(filePath);
        }
    }
}