﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PCLStorage;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace BygningsTinder
{
    class ReadWriteFiles
    {
        string filepath = "";

        public async Task CreateFile(string line)
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            IFolder folder = await rootFolder.CreateFolderAsync("Results", CreationCollisionOption.OpenIfExists);

            IFile file = await folder.CreateFileAsync(DependencyService.Get<IGetSerialNumber>().GetSerialNumber() + ".txt", CreationCollisionOption.ReplaceExisting);

            await file.WriteAllTextAsync(line);

            DependencyService.Get<IFtpWebRequest>().upload(file.Path);
        }

        public void UploadFileFTP()
        {
            
        }
    }
}
