﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
 
using Xamarin.Forms; 

namespace BygningsTinder
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public Array[] cardArray;

        public event PropertyChangedEventHandler PropertyChanged;

        static public List<CardStackView.Item> items = new List<CardStackView.Item>();
        public List<CardStackView.Item> ItemsList
        {
            get { return items; }
            set { if (items == value) { return; } items = value; OnPropertyChanged(); }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        static public void RemoveAtFirst()
        {
            items.RemoveAt(0);
        }

        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            field = value;
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static void Shuffle<T>(IList<T> array, Random rng, int first, int count)
        {
            for (int n = count; n > 1;)
            {
                int k = rng.Next(n);
                --n;
                T temp = array[n + first];
                array[n + first] = array[k + first];
                array[k + first] = temp;
            }
        }

        public MainPageViewModel()
        {
            items.Add(new CardStackView.Item() { Name = "Velkommen til Tjek Slagelse", firstDesc = "Her vil du se billeder, der viser forskellige måder at bruge byen og landet på.\n\nTil hvert billede hører et spørgsmål, og med dine svar fortæller du, hvad du godt kunne tænke dig mere eller mindre af.\n\nHvis du vil svare ja til spørgsmålet, skal du swipe billedet til højre eller trykke på det grønne flueben.\n\nHvis du vil svare nej, skal du swipe billedet til venstre eller trykke på det røde kryds.\n\nGod fornøjelse!", firstDesc2 = "Tjek Slagelse er et formidlingstiltag i forbindelse med den offentlige høring af Forslag til Kommuneplan 2017.", Photo = new Uri("https://focuslock.dk/xamarin/pixela.png") });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/cheFGa/01.jpg"), Description = "Kunst i byen", ID = 1, firstDesc = " ", firstDesc2 = " "});
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/mfE49v/02.jpg"), Description = "Vindmøller i det åbne land", ID = 2, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/fvLcUv/03.jpg"), Description = "At man kan sejle i kajak fra Storebælt til Trelleborg ?", ID = 3, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du gå på opdagelse her?", Photo = new Uri("https://image.ibb.co/jBV0hF/04.jpg"), Description = "Ture, motion og events i skovene", ID = 4, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/mWAcUv/05.jpg"), Description = "Klatrevæg som samlingspunkt i byen", ID = 5, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/nwwj9v/06.jpg"), Description = "Boliger og natur tæt på byen", ID = 6, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/e0rvGa/07.jpg"), Description = "Moderne rækkehuse i grønne omgivelser", ID = 7, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du hænge ud her?", Photo = new Uri("https://image.ibb.co/cUoxUv/08.jpg"), Description = "Havnekajen i Korsør", ID = 8, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du handle her?", Photo = new Uri("https://image.ibb.co/czhP9v/09.jpg"), Description = "Boder i torvehal", ID = 9, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du opgive parkeringspladser for det her?", Photo = new Uri("https://image.ibb.co/kE5cUv/10.jpg"), Description = "Cafemiljø i stedet for parkering i byen", ID = 10, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/dnK49v/11.jpg"), Description = "Markedsplads i byen", ID = 11, firstDesc = "", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have flere farverige facader?", Photo = new Uri("https://image.ibb.co/erf0hF/12.jpg"), Description = "", ID = 12, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bruge dem her?", Photo = new Uri("https://image.ibb.co/cfGvGa/13.jpg"), Description = "Bycykler", ID = 13, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo i en husbåd?", Photo = new Uri("https://image.ibb.co/hEbvGa/14.jpg"), Description = "Husbåd i Korsør eller Skælskør", ID = 14, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/k6f0hF/15.jpg"), Description = "At der kan plantes skov i store dele af kommunen?", ID = 15, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du deltage i det her?", Photo = new Uri("https://image.ibb.co/fNixUv/16.jpg"), Description = "Koncert i en park", ID = 16, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/ceoxUv/17.jpg"), Description = "Byhaver", ID = 17, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/dp1vGa/18.jpg"), Description = "Alternative legepladser", ID = 18, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/dz3mNF/19.jpg"), Description = "Et mindre byhus", ID = 19, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du deltage i det her?", Photo = new Uri("https://image.ibb.co/i8TaGa/20.jpg"), Description = "Koncert midt på gaden", ID = 20, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/hs4FGa/21.jpg"), Description = "Solcelleanlæg i det åbne land", ID = 21, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du holde ferie her?", Photo = new Uri("https://image.ibb.co/fTyxUv/22.jpg"), Description = "Campingferie", ID = 22, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du gå lidt længere for at parkere i et parkeringshus?", Photo = new Uri("https://image.ibb.co/nBkcUv/23.jpg"), Description = "Parkeringshus i byen", ID = 23, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/cPLxwa/24.jpg"), Description = "Kunst i byen", ID = 24, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/eEu1NF/25.jpg"), Description = "Boliger med udsigt til hav", ID = 25, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/f2WgNF/26.jpg"), Description = "At der er et vandsportscenter på vej i Korsør", ID = 26, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/iGB4ba/27.jpg"), Description = "Parcelhus-kvarter", ID = 27, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/bT0K9v/28.jpg"), Description = "Byhuse midt i byen", ID = 28, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/hXCAGa/29.jpg"), Description = "Parkouranlæg", ID = 29, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/hnXcwa/30.jpg"), Description = "Mindre byhuse", ID = 30, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/hxnAGa/31.jpg"), Description = "Gavlmalerier", ID = 31, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/cLY6pv/32.jpg"), Description = "Boldbane", ID = 32, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du handle her?", Photo = new Uri("https://image.ibb.co/bxD6pv/33.jpg"), Description = "Indkøbscenter", ID = 33, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du hænge ud her?", Photo = new Uri("https://image.ibb.co/ms7XUv/34.jpg"), Description = "Lystanlægget i Slagelse", ID = 34, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du handle her?", Photo = new Uri("https://image.ibb.co/nokVGa/35.jpg"), Description = "Slagelse Megacenter", ID = 35, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du handle her?", Photo = new Uri("https://image.ibb.co/cnggNF/36.jpg"), Description = "Algade, Skælskør", ID = 36, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/cBNAGa/37.jpg"), Description = "Anderledes arkitektur", ID = 37, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bo her?", Photo = new Uri("https://image.ibb.co/m4YvhF/38.jpg"), Description = "Kolonihavehus", ID = 38, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/myNXUv/39.jpg"), Description = "Farve i belægning", ID = 39, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du overnatte her?", Photo = new Uri("https://image.ibb.co/fg4e9v/40.jpg"), Description = "Shelters i det fri", ID = 40, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/dvampv/41.jpg"), Description = "Skaterpark", ID = 41, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bruge dem her?", Photo = new Uri("https://image.ibb.co/cXHo2F/42.jpg"), Description = "Leg i skoven", ID = 42, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/btWsUv/43.jpg"), Description = "Grønt i byen", ID = 43, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du gå på opdagelse her?", Photo = new Uri("https://image.ibb.co/kZXz9v/44.jpg"), Description = "Liv på landet", ID = 44, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bruge den her?", Photo = new Uri("https://image.ibb.co/hFgFhF/45.jpg"), Description = "Stander til opladning af mobiler", ID = 45, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du deltage i det her?", Photo = new Uri("https://image.ibb.co/mV5K9v/46.jpg"), Description = "Keramikfestival", ID = 46, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du gå på opdagelse her?", Photo = new Uri("https://image.ibb.co/nu0VGa/47.jpg"), Description = "Legepark", ID = 47, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du deltage i det her?", Photo = new Uri("https://image.ibb.co/mdT6pv/48.jpg"), Description = "Vikingefestival på Trelleborg", ID = 48, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/eOho2F/49.jpg"), Description = "At Slagelse Kommune har 180 km kystlinie?", ID = 49, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du prøve det her?", Photo = new Uri("https://image.ibb.co/fwsXUv/50.jpg"), Description = "Kite surfing", ID = 50, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du deltage i det her?", Photo = new Uri("https://image.ibb.co/fC5mpv/51.jpg"), Description = "En tur på rulleskøjtestadion", ID = 51, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du hænge ud her?", Photo = new Uri("https://image.ibb.co/nHE1NF/52.jpg"), Description = "Et arrangement på Trelleborg", ID = 52, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du hænge ud her?", Photo = new Uri("https://image.ibb.co/g8U1NF/53.jpg"), Description = "Siddemøbel i gyngestativ", ID = 53, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du synes om det her i byens gader?", Photo = new Uri("https://image.ibb.co/buf82F/54.jpg"), Description = "Farverige paraplyer eller lignende", ID = 54, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du hænge ud her?", Photo = new Uri("https://image.ibb.co/c4t6pv/55.jpg"), Description = "Schweizerpladsen i Slagelse", ID = 55, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Er du her i din fritid?", Photo = new Uri("https://image.ibb.co/mhdvhF/56.jpg"), Description = "På golfbanen", ID = 56, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du nyde udsigten her?", Photo = new Uri("https://image.ibb.co/fK64ba/57.jpg"), Description = "Marken og landskabet", ID = 57, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have mere af det her?", Photo = new Uri("https://image.ibb.co/fxVmpv/58.jpg"), Description = "Beplantning langs vejene", ID = 58, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du gå med her?", Photo = new Uri("https://image.ibb.co/g1rgNF/59.jpg"), Description = "Gåtur i naturen", ID = 59, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/eVojba/60.jpg"), Description = "At vi bliver flere borgere i kommunen?", ID = 60, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Mangler der mere af det her?", Photo = new Uri("https://image.ibb.co/cTtvhF/61.jpg"), Description = "Kunst i byen", ID = 61, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du være her i din fritid?", Photo = new Uri("https://image.ibb.co/gCQ82F/62.jpg"), Description = "Fiskerihavn", ID = 62, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Er du her i din fritid?", Photo = new Uri("https://image.ibb.co/dORFhF/63.jpg"), Description = "På atletikbanen", ID = 63, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/nuEPba/64.jpg"), Description = "At vi har et internationalt keramisk center i Skælskør?", ID = 64, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/dfJHwa/65.jpg"), Description = "At vi er Team Danmark Elitekommune?", ID = 65, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du med ud i vandet?", Photo = new Uri("https://image.ibb.co/i591NF/66.jpg"), Description = "På kig efter fisk", ID = 66, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du have flere bygninger med det her?", Photo = new Uri("https://image.ibb.co/gvkxwa/67.jpg"), Description = "Beplantning på facade", ID = 67, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vidste du…", Photo = new Uri("https://image.ibb.co/gBxAGa/68.jpg"), Description = "At vi planlægger for en bæredygtig bydel i Tidselbjerget?", ID = 68, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du bruge det her sted?", Photo = new Uri("https://image.ibb.co/eubRpv/69.jpg"), Description = "Motionsoase", ID = 69, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Vil du gå på opdagelse her? ", Photo = new Uri("https://image.ibb.co/gyq82F/70.jpg"), Description = "Naturpark langs dobbeltkysten", ID = 70, firstDesc = " ", firstDesc2 = " " });
            items.Add(new CardStackView.Item() { Name = "Tak for din deltagelse", firstDesc = "Billeder:\n\nSlagelse Kommune\nColourbox\nSlagelse Boligselskab\nJacob Nyborg Adreasen\n\n\n Udviklet af:\n\nSebastian Berg Rasmussen\nMalte Gejr Korup\n i samarbejde med\n\nSlagelse Tekniske Gymnasium\nSlagelse Kommune", Photo = new Uri("https://focuslock.dk/xamarin/pixela.png"), firstDesc2 = " " });

            Shuffle(items, new Random(), 1, items.Count - 2);
        }

        
    }
}
