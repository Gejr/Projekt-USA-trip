﻿using System;
using System.IO;
using System.Collections.Generic;
using Xamarin.Forms;

[assembly: Dependency(typeof(MasterDetailPage))]
namespace BygningsTinder
{

    public partial class MainPage : ContentPage
    {

        public static int screenHeight, screenWidth;
        CardStackView cardStack;
        MainPageViewModel viewModel = new MainPageViewModel();

        public interface ISaveAndLoad
        {
            void SaveText(string filename, string text);
            string LoadText(string filename);
        }

        public MainPage()
        {
            this.BindingContext = viewModel;
            this.BackgroundColor = Color.Black;

            RelativeLayout view = new RelativeLayout();

            //Finder de rigtige funktioner og linker dem til de matchene funktioner her
            cardStack = new CardStackView();
            cardStack.SetBinding(CardStackView.ItemsSourceProperty, "ItemsList");
            cardStack.SwipedLeft += SwipedLeft;
            cardStack.SwipedRight += SwipedRight;

            // knappen der bruges til at dislike billeder
            Button dislike_but = new Button()
            {
                Image = (FileImageSource)ImageSource.FromFile("Dislike.png"),
                Scale = 2,
                BackgroundColor = Color.Transparent,
                BorderColor = Color.Transparent,
                BorderRadius = 0,
                BorderWidth = 0
            };

            // knappen der bruges til at like billeder
            Button like_but = new Button()
            {
                Image = (FileImageSource)ImageSource.FromFile("Like.png"),
                Scale = 2,
                BackgroundColor = Color.Transparent,
                BorderColor = Color.Transparent,
                BorderRadius = 0,
                BorderWidth = 0
            };

            // CLICK event for begge knapper
            dislike_but.Clicked += Dislike_but_Clicked;
            like_but.Clicked += Like_but_Clicked;

            //Tilpasser det til skærmen
            view.Children.Add(cardStack,
                Constraint.Constant(30),
                Constraint.Constant(60),
                Constraint.RelativeToParent((parent) => { return parent.Width - 60; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 140; }));

            // Tilføjer dislike knappen til skærmen
            view.Children.Add(dislike_but,
                Constraint.RelativeToParent((parent) => { return parent.Width - parent.Width + 75; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 80; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.Constant(75),
                Constraint.Constant(50));

            // Tilføjer like knappen til skærmen
            view.Children.Add(like_but,
                Constraint.RelativeToParent((parent) => { return parent.Width - 135; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 80; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.Constant(75),
                Constraint.Constant(50));



            this.LayoutChanged += (object sender, EventArgs e) =>
            {
                cardStack.CardMoveDistance = (int)(this.Width / 3);
            };

            this.Content = view;
        }

        /// <summary>
        ///             SØRGER FOR AT SKIFTE TIL NÆSTE BILLEDE NÅR DER BLIVER LIKET
        /// </summary>
        private void Like_but_Clicked(object sender, EventArgs e)
        {
            SwipedRight(cardStack.itemIndex);
            cardStack.GetNextCard().Scale = 1;
            cardStack.ShowNextCard();
        }

        /// <summary>
        ///             SØRGER FOR AT SKIFTE TIL NÆSTE BILLEDE NÅR DER BLIVER DISLIKET
        /// </summary>
        private void Dislike_but_Clicked(object sender, EventArgs e)
        {
            SwipedLeft(cardStack.itemIndex);
            cardStack.GetNextCard().Scale = 1;
            cardStack.ShowNextCard();
        }

        // To arrays som holder styr på likes og dislikes
        int[] likes = new int[73];
        int[] dislikes = new int[73];

        List<int> ids = new List<int>();

        /// <summary>
        ///             SWIPET TIL HØJRE FUNKTION
        /// </summary>
        void SwipedRight(int index)
        {
            //Selve funktionen der skal ske når du har swipet til højre
            if (index != 0)
            {
                ids.Add(cardStack.ItemsSource[index - 1].ID);
            }
            likes[index]++;
            cardStack.GetNextCard().Scale = 1;
            cardStack.GetTopCard().Scale = 1;
            //DisplayAlert("AMOUNT OF LIKES/DISLIKES : " + index, string.Format("{0} Likes : {1} Dislikes", likes[index], dislikes[index]), "OK");
            if(index == 72)
            {
                cardStack.cards[cardStack.topCardIndex].firstDesc.Text = "Billeder:\n\nSlagelse Kommune\nColourbox\nSlagelse Boligselskab\nJacob Nyborg Adreasen\n\n\n Udviklet af:\n\nSebastian Berg Rasmussen\nMalte Gejr Korup\n i samarbejde med\n\nSlagelse Tekniske Gymnasium\nSlagelse Kommune";
                cardStack.GetNextCard().firstDesc.Text = "Billeder:\n\nSlagelse Kommune\nColourbox\nSlagelse Boligselskab\nJacob Nyborg Adreasen\n\n\n Udviklet af:\n\nSebastian Berg Rasmussen\nMalte Gejr Korup\n i samarbejde med\n\nSlagelse Tekniske Gymnasium\nSlagelse Kommune";
                //DisplayAlert("TAK!", "Det var alle kort, du kan stemme igen hvis det er", "OK");
                var write = new ReadWriteFiles();
                string text = "";
                for (int i = 0; i < ids.Count; i++)
                {
                    text += string.Format("Billede {0}: Likes: {1}, Dislikes: {2}", ids[i], likes[i], dislikes[i]) + Environment.NewLine;
                }
                write.CreateFile(text);
                write.UploadFileFTP();
                ids.Clear();
            }
            if(index == 1)
            {
                cardStack.cards[cardStack.topCardIndex].firstDesc.Text = "Her vil du se billeder, der viser forskellige måder at bruge byen og landet på.\n\nTil hvert billede hører et spørgsmål, og med dine svar fortæller du, hvad du godt kunne tænke dig mere eller mindre af.\n\nHvis du vil svare ja til spørgsmålet, skal du swipe billedet til højre eller trykke på det grønne flueben.\n\nHvis du vil svare nej, skal du swipe billedet til venstre eller trykke på det røde flueben.\n\n\nGod fornøjelse!";
                cardStack.cards[cardStack.topCardIndex].firstDesc2.Text = "Tjek Slagelse er et formidlingstiltag i forbindelse med den offentlige høring af Forslag til Kommuneplan 2017.";
                cardStack.GetNextCard().firstDesc.Text = "Her vil du se billeder, der viser forskellige måder at bruge byen og landet på.\n\nTil hvert billede hører et spørgsmål, og med dine svar fortæller du, hvad du godt kunne tænke dig mere eller mindre af.\n\nHvis du vil svare ja til spørgsmålet, skal du swipe billedet til højre eller trykke på det grønne flueben.\n\nHvis du vil svare nej, skal du swipe billedet til venstre eller trykke på det røde flueben.\n\n\nGod fornøjelse!";
                cardStack.GetNextCard().firstDesc2.Text = "Tjek Slagelse er et formidlingstiltag i forbindelse med den offentlige høring af Forslag til Kommuneplan 2017.";
            }
            if(index >= 1)
            {
                cardStack.cards[cardStack.topCardIndex].firstDesc.Text = "";
                cardStack.cards[cardStack.topCardIndex].firstDesc2.Text = "";
                cardStack.cards[cardStack.topCardIndex].Scale = 1;
                cardStack.GetNextCard().Scale = 1;
            }

        }

        /// <summary>
        ///             SWIPET TIL VENSTRE FUNKTION
        /// </summary>
        void SwipedLeft(int index)
        {
            //Selve funktionen der skal ske når du har swipet til venstre
            if (index != 0)
            {
                ids.Add(cardStack.ItemsSource[index-1].ID);
            }
            dislikes[index]++;
            cardStack.GetNextCard().Scale = 1;
            cardStack.GetTopCard().Scale = 1;
            //DisplayAlert("AMOUNT OF LIKES/DISLIKES : " + cardStack.ItemsSource[index-1].ID, string.Format("{0} Likes : {1} Dislikes", likes[index], dislikes[index]), "OK");
            if (index == 72)
            {
                cardStack.cards[cardStack.topCardIndex].firstDesc.Text = "Billeder:\n\nSlagelse Kommune\nColourbox\nSlagelse Boligselskab\nJacob Nyborg Adreasen\n\n\n Udviklet af:\n\nSebastian Berg Rasmussen\nMalte Gejr Korup\n i samarbejde med\n\nSlagelse Tekniske Gymnasium\nSlagelse Kommune";
                cardStack.GetNextCard().firstDesc.Text = "Billeder:\n\nSlagelse Kommune\nColourbox\nSlagelse Boligselskab\nJacob Nyborg Adreasen\n\n\n Udviklet af:\n\nSebastian Berg Rasmussen\nMalte Gejr Korup\n i samarbejde med\n\nSlagelse Tekniske Gymnasium\nSlagelse Kommune";
                //DisplayAlert("TAK!", "Det var alle kort, du kan stemme igen hvis det er", "OK");
                var write = new ReadWriteFiles();
                string text = "";
                for (int i = 0; i < ids.Count; i++)
                {
                    text += string.Format("Billede {0}: Likes: {1}, Dislikes: {2}", ids[i], likes[i], dislikes[i]) + Environment.NewLine;
                }
                write.CreateFile(text);
                write.UploadFileFTP();
                ids.Clear();
            }
            if (index == 1)
            {
                cardStack.cards[cardStack.topCardIndex].firstDesc.Text = "Her vil du se billeder, der viser forskellige måder at bruge byen og landet på.\n\nTil hvert billede hører et spørgsmål, og med dine svar fortæller du, hvad du godt kunne tænke dig mere eller mindre af.\n\nHvis du vil svare ja til spørgsmålet, skal du swipe billedet til højre eller trykke på det grønne flueben.\n\nHvis du vil svare nej, skal du swipe billedet til venstre eller trykke på det røde flueben.\n\n\nGod fornøjelse!";
                cardStack.cards[cardStack.topCardIndex].firstDesc2.Text = "Tjek Slagelse er et formidlingstiltag i forbindelse med den offentlige høring af Forslag til Kommuneplan 2017.";
                cardStack.GetNextCard().firstDesc.Text = "Her vil du se billeder, der viser forskellige måder at bruge byen og landet på.\n\nTil hvert billede hører et spørgsmål, og med dine svar fortæller du, hvad du godt kunne tænke dig mere eller mindre af.\n\nHvis du vil svare ja til spørgsmålet, skal du swipe billedet til højre eller trykke på det grønne flueben.\n\nHvis du vil svare nej, skal du swipe billedet til venstre eller trykke på det røde flueben.\n\n\nGod fornøjelse!";
                cardStack.GetNextCard().firstDesc2.Text = "Tjek Slagelse er et formidlingstiltag i forbindelse med den offentlige høring af Forslag til Kommuneplan 2017.";
            }
            if (index >= 1)
            {
                cardStack.cards[cardStack.topCardIndex].firstDesc.Text = "";
                cardStack.cards[cardStack.topCardIndex].firstDesc2.Text = "";
                cardStack.cards[cardStack.topCardIndex].Scale = 1;
                cardStack.GetNextCard().Scale = 1;
            }
        }
    }
}
