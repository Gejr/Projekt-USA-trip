﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace BygningsTinder
{
    public class CardView : ContentView
    {
        public Label Name { get; set; }
        public Image Photo { get; set; }
        public Label Location { get; set; }
        public Label Description { get; set; }
        public Label firstDesc { get; set; }
        public Label firstDesc2 { get; set; }
        public int ID { get; set; }
        public Image Like { get; set; }
        public Image DisLike { get; set; }

        public CardView()
        {
            RelativeLayout view = new RelativeLayout();

            /// 
            /// BAGERSTE BAGGRUND
            /// 
            BoxView background = new BoxView
            {
                Color = Color.White,
                Scale = 4,
                InputTransparent = true
            };
            //Tilpasser det til skærmen
            view.Children.Add(background,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return parent.Height; }));

            /// 
            /// KORTETS BAGGRUND
            /// 
            BoxView boxView1 = new BoxView
            {
                Color = Color.FromRgb(245, 245, 245),
                InputTransparent = true
            };
            //Tilpasser det til skærmen
            view.Children.Add(boxView1,
                Constraint.Constant(0),
                Constraint.Constant(-30),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return parent.Height; }));

            /// 
            /// BILLEDET
            /// 
            Photo = new Image()
            {
                InputTransparent = true,
                Aspect = Aspect.Fill,
                Scale = 0.95
            };
            //Tilpasser det til skærmen
            view.Children.Add (Photo,
                Constraint.RelativeToParent((parent) => { double w = parent.Width * 1; return ((parent.Width - w) / 2); }),
                Constraint.Constant(10),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return (parent.Height * 0.75); }));


            /// 
            /// TITEL
            /// 
            Name = new Label()
            {
                TextColor = Color.Black,
                FontSize = MainPage.screenHeight / 36.8,
                InputTransparent = true,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center
            };
            //Tilpasser det til skærmen
            view.Children.Add(Name,
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => { return parent.Height - parent.Height - 30; }),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.Constant(65));


            /// 
            /// KORT BESKRIVELSE
            /// 
            Description = new Label()
            {
                TextColor = Color.Black,
                FontSize = MainPage.screenHeight / 40.8, //Font size 18 40.8
                InputTransparent = true
            };
            //Tilpasser det til skærmen
            view.Children.Add(Description,
                Constraint.RelativeToParent((parent) => { return parent.Width - parent.Width + 5; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 80; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.Constant(68));

            /// 
            /// FØRSTE KORTS BESKRIVELSE
            /// 
            firstDesc = new Label()
            {
                TextColor = Color.Black,
                FontSize = MainPage.screenHeight / 36.8, //Font size 20
                InputTransparent = true
            };
            //Tilpasser det til skærmen
            view.Children.Add(firstDesc,
                Constraint.RelativeToParent((parent) => { return parent.Width - parent.Width + 5; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - parent.Height + 10; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.Constant(500));

            /// 
            /// FØRSTE KORTS ANDEN BESKRIVELSE
            /// 
            firstDesc2 = new Label()
            {
                TextColor = Color.DimGray,
                FontSize = 12,
                InputTransparent = true,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center
            };
            //Tilpasser det til skærmen
            view.Children.Add(firstDesc2,
                Constraint.RelativeToParent((parent) => { return parent.Width - parent.Width + 5; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 85; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return parent.Height; }));

            ///
            /// LIKE TEKST
            ///
            Like = new Image()
            {
                Opacity = 0,
                Scale = 2,
                InputTransparent = true,
                Source = ImageSource.FromFile("Like_NoWhite")
            };
            view.Children.Add(Like,
                Constraint.RelativeToParent((parent) => { return parent.Width - parent.Width - 105; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 445; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.Constant(68));

            ///
            /// DISLIKE TEKST
            /// 
            DisLike = new Image()
            {
                Opacity = 0,
                Scale = 2,
                InputTransparent = true,
                Source = ImageSource.FromFile("Dislike_NoWhite")
            };
            view.Children.Add(DisLike,
                Constraint.RelativeToParent((parent) => { return parent.Width - 190; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - 435; }),
                //Constraint.RelativeToParent((parent) => { return parent.Height - 80; }), //MIDDLE
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.Constant(68));

            //putter det hele på skærmen
            Content = view;

        }
    }
}