﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace BygningsTinder
{
    public class CardStackView : ContentView
    {
        public class Item
        {
            public string Name { get; set; }
            public Uri Photo { get; set; }
            public string Description { get; set; }
            public string firstDesc { get; set; }
            public string firstDesc2 { get; set; }
            public int ID { get; set; }
        }


        //Bagved liggende kort's scale
        const float BackCardScale = 0.8f;

        //Længden af animationer
        //Jo højere = langsommere effekt
        const int AnimLength = 300;

        //Xamarin arbejder bedre med Radians end Degrees
        const float DegreesToRadians = 57.2957795f; // <-- 180 / pi

        //Jo højere = mindre rotations effekt
        const float CardRotationAdjuster = 0.3f;

        //En længde kortet skal flyttes før den registres som like/dislike
        //Længden bliver deffineret i MainPage.xaml.cs - linje 33
        public int CardMoveDistance { get; set; }


        //Hvor mange kort skal der være i Arraylisten
        const int NumCards = 2;
        //Selve Arrayet
        public CardView[] cards = new CardView[NumCards];

        //Kortet som ligger øverst i arrayet
        public int topCardIndex;

        //Distancen kortet er blevet flyttet
        float cardDistance = 0;

        //Den sidste item som er blevet added til kort bunken
        public int itemIndex = 0;
        bool ignoreTouch = false;


        //Bliver kaldet når et kort bliver swipet til venstre/højre
        public Action<int> SwipedRight = null;
        public Action<int> SwipedLeft = null;

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(System.Collections.IList), typeof(CardStackView), null, propertyChanged: OnItemsSourcePropertyChanged);


        public List<Item> ItemsSource
        {
            get
            {
                return (List<Item>)GetValue(ItemsSourceProperty);
            }

            set
            {
                SetValue(ItemsSourceProperty, value);
                itemIndex = 0;
            }
        }

        private static void OnItemsSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((CardStackView)bindable).Setup();
        }

        public CardStackView()
        {

            RelativeLayout view = new RelativeLayout();

            //Laver selve kort bunken
            for (int i = 0; i < NumCards; i++)
            {
                var card = new CardView();
                cards[i] = card;
                card.InputTransparent = true;
                card.IsVisible = false;

                view.Children.Add(card,
                    Constraint.Constant(0),
                    Constraint.Constant(0),
                    Constraint.RelativeToParent((parent) => { return parent.Width; }),
                    Constraint.RelativeToParent((parent) => { return parent.Height; }));
            }

            this.BackgroundColor = Color.Black;
            this.Content = view;

            var panGesture = new PanGestureRecognizer();
            panGesture.PanUpdated += OnPanUpdated;
            GestureRecognizers.Add(panGesture);

            var tapGesture = new TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;
            GestureRecognizers.Add(tapGesture);
        }

        private void TapGesture_Tapped(object sender, EventArgs e)
        {
            //int tmp = ItemsSource[itemIndex].ID;
            //tmp -= 2;
            //Page p = new Page();
            //p.DisplayAlert("heiii", tmp.ToString(),"hej");
            //Toast.MakeText((Android.Content.Context)this,tmp.ToString(), ToastLength.Short).Show();
        }

        void Setup()
        {
            //Sætter top kortet
            topCardIndex = 0;

            //Laver en bunke med kort
            for(int i = 0; i < Math.Min(NumCards, ItemsSource.Count); i++)
            {
                if(itemIndex >= ItemsSource.Count)
                {
                    break;
                }
                var card = cards[i];
                card.Name.Text = ItemsSource[itemIndex].Name;
                card.Description.Text = ItemsSource[itemIndex].Description;
                card.firstDesc.Text = ItemsSource[itemIndex].firstDesc;
                card.firstDesc2.Text = ItemsSource[itemIndex].firstDesc2;
                card.Photo.Source = ImageSource.FromUri(ItemsSource[itemIndex].Photo); //ImageSource.FromFile(ItemsSource[itemIndex].Photo);
                card.ID = ItemsSource[itemIndex].ID;
                card.IsVisible = true;
                card.Scale = GetScale(i);
                card.RotateTo (0, 0);
                card.TranslateTo(0, -card.Y, 0);
                ((RelativeLayout)this.Content).LowerChild(card);
                itemIndex++;
            }
        }

        void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    Handle_TouchStart();
                    break;

                case GestureStatus.Running:
                    Handle_Touch((float)e.TotalX);
                    break;

                case GestureStatus.Completed:
                    Handle_TouchEnd();
                    break;
            }
        }

        //Handler når et touch event starter
        public void Handle_TouchStart()
        {
            cardDistance = 0;
            //cards[NextCardIndex(topCardIndex)].firstDesc.Text = "";
            //cards[NextCardIndex(topCardIndex)].firstDesc2.Text = "";
        }

        //Handler når der er et touch event igang
        public void Handle_Touch(float diff_x)
        {
            if (ignoreTouch)
            {
                return;
            }

            var topCard = cards[topCardIndex];
            var backCard = cards[PrevCardIndex(topCardIndex)];

            //Bevæger top kortet
            if(topCard.IsVisible)
            {
                //Bevæger kortet
                topCard.TranslationX = (diff_x);

                //Udregner vinkelen på kortet (hældningen)
                float rotationAngel = (float)(CardRotationAdjuster * Math.Min(diff_x / this.Width, 1.0f));
                topCard.Rotation = rotationAngel * DegreesToRadians;
                if (diff_x < 0)
                {
                    topCard.DisLike.Opacity = -(diff_x) / 100;
                }
                if (diff_x > 0)
                {
                    topCard.Like.Opacity = (diff_x)/100;
                }

                //Gemmer hvor langt den har bevæget sig
                cardDistance = diff_x;
            }

            //Scaler det bagved liggende kort
            if (backCard.IsVisible)
            {
                backCard.Scale = Math.Min(BackCardScale + Math.Abs((cardDistance / CardMoveDistance) * (1.0f - BackCardScale)), 1.0f);
            }
        }

        async public void Handle_TouchEnd()
        {
            ignoreTouch = true;

            cards[topCardIndex].Like.Opacity = 0;
            cards[topCardIndex].DisLike.Opacity = 0;

            var topCard = cards[topCardIndex];

            //Bliver kørt hvis kortet blev flyttet langt nok til at blive set som swipet
            if (Math.Abs ((int)cardDistance) > CardMoveDistance)
            {
                //Flytter kortet af skærmen
                await topCard.TranslateTo(cardDistance > 0 ? this.Width : -this.Width, 0, AnimLength / 2, Easing.SpringOut);
                topCard.IsVisible = false;

                if(SwipedRight != null && cardDistance > 0)
                {
                    SwipedRight(itemIndex);
                    //SwipedRight(itemIndex);
                }
                else if (SwipedLeft != null)
                {
                    SwipedLeft(itemIndex);
                    //SwipedLeft(itemIndex);
                }

                //Viser det næste kort
                ShowNextCard();
            }

            //Putter kortet tilbage i midten hvor det startede
            else
            {
                //Flytter kortet
                await topCard.TranslateTo((-topCard.X), -topCard.Y, AnimLength, Easing.SpringOut);
                await topCard.RotateTo(0, AnimLength, Easing.SpringOut);

                //Scale kortet tilbage igen
                var prevCard = cards[PrevCardIndex(topCardIndex)];
                await prevCard.ScaleTo(BackCardScale, AnimLength, Easing.SpringOut);
            }

            ignoreTouch = false;
        }

        public void ShowNextCard()
        {
            if (cards[0].IsVisible == false && cards[1].IsVisible == false)
            {
                Setup();
                return;
            }

            var topCard = cards[topCardIndex];
            topCardIndex = NextCardIndex(topCardIndex);

            if (itemIndex == ItemsSource.Count)
            {
                
                itemIndex = 0;
            }

            //Hvis der er flere kort som den kan vise så viser vi det nye kort der hvor det tidligere kort var
            if(itemIndex < ItemsSource.Count)
            {
                ((RelativeLayout)this.Content).LowerChild(topCard);

                topCard.Scale = BackCardScale;
                topCard.RotateTo(0, 0);
                topCard.TranslateTo(0, -topCard.Y, 0);

                topCard.Name.Text = ItemsSource[itemIndex].Name;
                topCard.Description.Text = ItemsSource[itemIndex].Description;
                topCard.Photo.Source = ImageSource.FromUri(ItemsSource[itemIndex].Photo);

                topCard.IsVisible = true;
                itemIndex++;
            }
        }

        //giver det næste kort fra toppen
        int NextCardIndex(int topIndex)
        {
            return topIndex == 0 ? 1 : 0;
        }

        //Får det næste kort i bunken som et CardView
        public CardView GetNextCard()
        {
            var backCard = cards[PrevCardIndex(topCardIndex)];
            return backCard;
        }

        //giver det tidligere kort fra toppen
        int PrevCardIndex(int topIndex)
        {
            return topIndex == 0 ? 1 : 0;
        }

        //Hjælper function der hjælper med at finde scale
        float GetScale(int index)
        {
            return (index == topCardIndex) ? 1.0f : BackCardScale;
        }

        public CardView GetTopCard()
        {
            return cards[topCardIndex];
        }
    }
}
