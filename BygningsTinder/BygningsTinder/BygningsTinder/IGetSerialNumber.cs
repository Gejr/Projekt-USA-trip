﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BygningsTinder
{
    public interface IGetSerialNumber
    {
        string GetSerialNumber();
    }
}
