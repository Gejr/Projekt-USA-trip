﻿using System.Threading.Tasks;

namespace BygningsTinder
{
    public interface IFileSystem
    {
        Task WriteTextAsync(string fileName, string Text);
    }
}