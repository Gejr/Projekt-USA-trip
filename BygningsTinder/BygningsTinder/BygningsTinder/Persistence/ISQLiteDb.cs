﻿using SQLite;
namespace BygningsTinder
{
    public interface ISQLiteDb
    {
        SQLiteAsyncConnection GetConnection();
    }
}